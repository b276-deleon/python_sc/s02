year = 0

while True:
    try:
        year = int(input("Enter a year:\n"))
    except:
        print("Please enter a valid year")
        continue
    if year <= 0 :
        print("Please enter a valid year")
    elif year % 400 == 0 or year % 100 != 0 and year % 4 == 0 :
        print(f"{year} is a leap year!")
        break
    else:
        print(f"{year} is NOT a leap year!")
        break

# ----------------------

row = int(input("Enter number of rows:\n"))
col = int(input("Enter number of columns:\n"))

for r in range(row):
    for c in range(col):
        print("*", end = "")
    print()

